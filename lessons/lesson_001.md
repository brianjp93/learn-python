# what is python?
- compiled vs interpreted languages
- python is an interpreted language, you need to run a program to read your python code and convert it to machine code during execution

# downloading python

There are many ways to install, python.  This is one of the more confusing things about using python and coding in general.
- conda
- poetry
- pyenv
- pip
- pip-tools
- pipenv
- more...
These are all tools that have to do with installing python and python dependency management.

### conda
- we will use conda.
	- I'm familiar with it
	- it was originally made for people in the realm of data science, although that's less of a draw for it now
- miniconda install
- [miniconda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/macos.html)

### editor
- vscode
	- it just works (tm)

If we had it my way, you would use vim/neovim, but I won't subject you to that yet.


# python basics (built-in types)
- str
- int/float
- list/tuple
- dict
- booleans


# math
- Exponent
- **

- Modulus/remainder
- %

- Integer division
- //

- Division
- /

- Multiplication
- *

- Subtraction
- -

- Addition
- +


# booleans and conditionals
```python
x = 'hello'
if x == 'world':
    print('hello world')
else:
    print('goodbye world')
```
- Comparing
- &lt;, &lte;
- &gt;, &gte;
- !=
- ==
- is
- not


# build a magic 8 ball


# jupyter notebooks


# functions
- reusable pieces of code
- take arguments, output any data type you want.
- defining a function
- calling a function
- return vs print
- def hello(name)


# write scripts to deal with data
- the movies dataset?
- find the highest rated movie on the list
- find the most popular movie

- find the highest rated movie in a specific year (use a function to allow us to select a year)
