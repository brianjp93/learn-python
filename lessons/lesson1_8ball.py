# automate the boring stuff
# https://automatetheboringstuff.com/2e/chapter4/

# Use a message list and the random module to get a random answer


messages = [
    'It is certain',
    'It is decidedly so',
    'Yes definitely',
    'Reply hazy try again',
    'Ask again later',
    'Concentrate and ask again',
    'My reply is no',
    'Outlook not so good',
    'Very doubtful',
]
