# what is python?
- compiled vs interpreted languages
- python is an interpreted language, you need to run a program to read your python code and convert it to machine code during execution

# downloading python

There are many ways to install, python.  This is one of the more confusing things about using python and coding in general.
- conda
- poetry
- pyenv
- pip
- pip-tools
- pipenv
- more...
These are all tools that have to do with installing python and python dependency management.

### conda
- we will use conda.
	- I'm familiar with it
	- it was originally made for people in the realm of data science, although that's less of a draw for it now
- miniconda install
- [miniconda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/macos.html)

### editor
- vscode
	- it just works (tm)

If we had it my way, you would use vim/neovim, but I won't subject you to that yet.


# python basics (built-in types)
- str
- int/float
- list/tuple
- dict


# dealing with data
- the movies dataset?
